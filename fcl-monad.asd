(defsystem "fcl-monad"
  :version "0.1.0"
  :author "ekatsym"
  :license "LLGPL"
  :components ((:module "src"
                :components
                ((:file "package")
                 (:file "util")
                 (:file "monad"       :depends-on ("package" "util"))
                 (:file "monad-plus"  :depends-on ("package" "util"))
                 (:file "functor"     :depends-on ("package" "util"))
                 (:file "applicative" :depends-on ("package" "util"))
                 (:file "sugar"       :depends-on ("package" "util" "monad")))))
  :description ""
  :in-order-to ((test-op (test-op "fcl-monad/tests"))))

(defsystem "fcl-monad/tests"
  :author "ekatsym"
  :license "LLGPL"
  :depends-on ("fcl-monad"
               "fcl-datatype"
               "fiveam")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for fcl-monad"
  :perform (test-op (o s) (uiop:symbol-call :fiveam '#:run!
                                            (uiop:find-symbol* '#:fcl-monad
                                                               :fcl-monad/tests))))
