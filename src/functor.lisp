(defpackage fcl-monad.functor
  (:use :cl)
  (:import-from
    :fcl-monad
    #:fmap))
(in-package :fcl-monad.functor)


(defgeneric fmap (function functor)
  (:documentation
"A method of functor.
Returns the functor including (apply function args)
where args is a content of (cons FUNCTOR MORE-FUNCTORS).
FMAP should satisfy the following rules:
  - (fmap #'identity x) == (identity x)
  - (fmap (compose g f) x) == (fmap g (fmap f x))
    where #'compose = (lambda (g f) (lambda (x) (g (f x))))"))
