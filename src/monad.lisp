(defpackage fcl-monad.monad
  (:use :cl)
  (:import-from
    :fcl-monad
    ;; Monad Core
    #:unit #:mfuncall)
  (:import-from
    :fcl-monad.util
    #:nlist?))
(in-package :fcl-monad.monad)


(defgeneric unit (class x)
  (:documentation
"A method of monad.
Returns a monad including x.
UNIT and MNDCALL should satisfy the following rules:
  - (mfuncall f (unit class x)) == (f x)
  - (mfuncall (lambda (x) (unit class x)) m) == m
  - (mfuncall g (mfuncall f m)) == (mfuncall (lambda (x) (mfuncall g (f x))) m)"))

(defgeneric mfuncall (x->monad monad)
  (:documentation
"A method of monad.
Returns (funcall x->monad x) where x is a content of monad.
UNIT and MNDCALL should satisfy the following rules:
  - (mfuncall f (unit class x)) == (f x)
  - (mfuncall (lambda (x) (unit class x)) m) == m
  - (mfuncall g (mfuncall f m)) == (mfuncall (lambda (x) (mfuncall g (f x))) m)"))
