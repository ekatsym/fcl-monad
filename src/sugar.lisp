(defpackage fcl-monad.sugar
  (:use :cl)
  (:import-from
    :fcl-monad
    #:mprogn #:mlet #:mdo #:listc
    #:unit #:mfuncall #:guard)
  (:import-from
    :fcl-monad.util
    #:nlist?))
(in-package :fcl-monad.sugar)


(defmacro mprogn (&rest monads)
  (let ((g!_ (gensym "G!_")))
    (reduce (lambda (m body)
              `(mfuncall (lambda (,g!_) (declare (ignore ,g!_)) ,body) ,m))
            monads
            :from-end t)))

(defmacro mlet ((&rest bindings) &body body)
  (every (lambda (binding)
           (check-type binding list)
           (assert (nlist? 2 binding) (binding))
           (print 0))
         bindings)
  (reduce (lambda (binding monad)
            (destructuring-bind (v m) binding
              `(mfuncall (lambda (,v) ,monad) ,m)))
          bindings
          :initial-value `(progn ,@body)
          :from-end t))

(defmacro mdo (&rest clauses)
  (reduce (lambda (clause body)
            (if (listp clause)
                (case (first clause)
                  (:in       (assert (nlist? 3 clause) (clause))
                             `(mlet ((,(second clause) ,(third clause))) ,body))
                  (:is       (assert (nlist? 3 clause) (clause))
                             `(let ((,(second clause) ,(third clause))) ,body))
                  (otherwise `(mprogn ,clause ,body)))
                `(mprogn ,clause ,body)))
          clauses
          :from-end t))

(defmacro listc (element &body clauses)
  (reduce (lambda (clause body)
            (if (listp clause)
                (case (first clause)
                  (:in       (assert (nlist? 3 clause) (clause))
                             `(mlet ((,(second clause) ,(third clause))) ,body))
                  (:is       (assert (nlist? 3 clause) (clause))
                             `(let ((,(second clause) ,(third clause))) ,body))
                  (otherwise `(mprogn (guard 'list ,clause) ,body)))
                `(mprogn (guard 'list ,clause) ,body)))
          clauses
          :initial-value `(unit 'list ,element)
          :from-end t))
