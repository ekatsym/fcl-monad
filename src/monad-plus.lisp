(defpackage fcl-monad.monadplus
  (:use :cl)
  (:import-from
    :fcl-monad
    #:mzero #:mplus
    #:unit
    ))
(in-package :fcl-monad.monadplus)


(defgeneric mzero (class)
  (:documentation
"A method of monad plus.
Returns an identity of mplus.
MZERO should satisfy the following rules:
  - (mfuncall f (mzero class)) == (mzero class)
  - (mprogn x (mzero class)) == (mzero class)"))

(defgeneric mplus (monad-plus1 monad-plus2)
  (:documentation
"A method of monoid.
Returns ``sum'' of MONAD-PLUS1 and MONAD-PLUS2.
MPLUS and MZERO should satisfy the following rules:
  - (mplus x (mzero class)) == (mplus (mzero class) x) == x
  - (mplus x (mplus y z)) == (mplus (mplus x y) z)"))

(defmethod mplus ((monad-plus1 null) monad-plus2)
  monad-plus2)

(defmethod mplus (monad-plus1 (monad-plus2 null))
  monad-plus1)

(defun guard (class test)
  (if test
      (unit class nil)
      (mzero class)))
