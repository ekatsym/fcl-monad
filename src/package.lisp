(defpackage fcl-monad
  (:export
    ;; Monad
    #:unit #:mfuncall

    ;; Functor
    #:fmap

    ;; Applicative
    #:afuncall

    ;; Monad Plus
    #:mzero #:mplus #:mplus*
    #:guard

    ;;; Syntax Sugar
    #:mprogn #:mlet #:mdo #:listc))
