(defpackage fcl-monad.util
  (:use :cl)
  (:export
    #:index
    #:nlist?))
(in-package :fcl-monad.util)


(deftype index ()
  `(integer 0 ,array-total-size-limit))

(defun nlist? (n list)
  (check-type n index)
  (check-type list list)
  (do ((i n (1- i))
       (lst list (rest lst)))
      ((or (zerop i) (null lst))
       (and (zerop i) (null lst)))
      (check-type lst list)))
