(defpackage fcl-monad.applicative
  (:use :cl)
  (:import-from
    :fcl-monad
    #:afuncall))
(in-package :fcl-monad.applicative)


(defgeneric afuncall (afunction applicative)
  (:documentation
"A method of applicative functor.
Returns the applicative functor including (apply AFUNCTION APPLICATIVE)
UNIT and AFUNCALL should satisfy the following rules:
  - (afuncall (unit class #'identity) v) == v
  - (afuncall (afuncall (unit class #'compose) u v) w) == (afuncall u (afuncall v w))
    where #'compose == (lambda (g f) (lambda (x) (g (f x))))
  - (afuncall u (unit class y)) == (afuncall (unit class (lambda (f) (funcall f y))) u)"))
