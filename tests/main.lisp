(defpackage fcl-monad.tests.main
  (:use :cl
        :fcl-datatype
        :fcl-monad
        :fiveam))
(in-package :fcl-monad.tests.main)

;;; Definition of Datatypes
;;; Maybe
(defdata maybe
  (nothing)
  (just t))

(defmethod unit ((class (eql 'maybe)) x)
  (just x))

(defmethod mfuncall (x->monad (monad maybe))
  (check-type x->monad function)
  (typecase monad
    (nothing   (nothing))
    (otherwise (funcall x->monad (just%0 monad)))))

(defmethod fmap (function (functor maybe))
  (check-type function function)
  (typecase functor
    (nothing   (nothing))
    (otherwise (just (funcall function (just%0 functor))))))

(defmethod afuncall (afunction (applicative maybe))
  (check-type afunction maybe)
  (typecase afunction
    (nothing   (nothing))
    (otherwise (typecase applicative
                 (nothing   (nothing))
                 (otherwise (just (funcall (just%0 afunction) (just%0 applicative))))))))

(defmethod mzero ((class (eql 'maybe)))
  (nothing))

(defmethod mplus ((monad-plus1 maybe) monad-plus2)
  (check-type monad-plus2 maybe)
  (typecase monad-plus1
    (nothing   monad-plus2)
    (otherwise (typecase monad-plus2
                 (nothing   monad-plus1)
                 (otherwise (just (mplus (just%0 monad-plus1) (just%0 monad-plus2))))))))

;;; Either
(defdata either
  (left t)
  (right t))

(defmethod unit ((class (eql 'either)) x)
  (right x))

(defmethod mfuncall (x->monad (monad either))
  (check-type x->monad function)
  (typecase monad
    (left      monad)
    (otherwise (funcall x->monad (right%0 monad)))))

(defmethod fmap (function (functor either))
  (check-type function function)
  (typecase functor
    (left      functor)
    (otherwise (right (funcall function (right%0 functor))))))

(defmethod afuncall (afunction (applicative either))
  (check-type afunction either)
  (typecase afunction
    (left      afunction)
    (otherwise (typecase applicative
                 (left      applicative)
                 (otherwise (right (funcall (right%0 afunction) (right%0 applicative))))))))

(defmethod mzero ((class (eql 'either)))
  (left nil))

(defmethod mplus ((monad-plus1 either) monad-plus2)
  (check-type monad-plus2 either)
  (typecase monad-plus1
    (left      monad-plus2)
    (otherwise (typecase monad-plus2
                 (left      monad-plus1)
                 (otherwise (right (mplus (right%0 monad-plus1) (right%0 monad-plus2))))))))

;;; List
(defmethod unit ((class (eql 'list)) x)
  (list x))

(defmethod mfuncall (x->monad (monad list))
  (check-type x->monad function)
  (reduce (lambda (x acc) (append (funcall x->monad x) acc))
          monad
          :initial-value '()
          :from-end t))

(defmethod fmap (function (monad list))
  (check-type function function)
  (mapcar function monad))

(defmethod afuncall (afunction (applicative list))
  (check-type afunction list)
  (reduce (lambda (f acc1)
            (reduce (lambda (x acc2) (append (funcall f x) acc2))
                    applicative
                    :initial-value acc1
                    :from-end t))
          afunction
          :initial-value '()
          :from-end t))

(defmethod mzero ((class (eql 'list)))
  '())

(defmethod mplus ((monad-plus1 list) monad-plus2)
  (check-type monad-plus2 list)
  (append monad-plus1 monad-plus2))

;;; Writer
(defdata writer
  (%writer t t))

(defun run-writer (writer)
  (check-type writer writer)
  (values (%writer%0 writer) (%writer%1 writer)))

(defmethod unit ((class (eql 'writer)) x)
  (%writer x nil))

(defmethod mfuncall (x->monad (monad writer))
  (multiple-value-bind (x1 w1) (run-writer monad)
    (multiple-value-bind (x2 w2) (run-writer (funcall x->monad x1))
      (%writer x2 (mplus w0 w1)))))

(defun fmap (function (functor writer))
  (check-type function function)
  (multiple-value-bind (x w) (run-writer functor)
    (%writer (funcall function x) w)))

(defun afuncall (afunction (applicative writer))
  (check-type writer afunction)
  (multiple-value-bind (f w1) (run-writer afunction)
    (multiple-value-bind (x w2) (run-writer applicative)
      (%writer (funcall f x) (mplus w1 w2)))))

;;; Function (Reader)
(defmethod unit ((class ((eql 'function))) x)
  (lambda (&rest _) (declare (ignore _)) x))

(defmethod unit ((class ((eql 'reader))) x)
  (lambda (&rest _) (declare (ignore _)) x))

(defmethod mfuncall (x->monad (monad function))
  (check-type x->monad function)
  (lambda (&rest args) (apply x->monad (apply monad args) args)))

(defmethod fmap (function (functor function))
  (check-type function function)
  (lambda (&rest args) (funcall function (apply functor args))))

(defmethod afuncall (afunction (applicative function))
  (check-type afunction function)
  (lambda (&rest args) (funcall (funcall afunction) (funcall applicative))))


(def-suite fcl-monad)
(in-suite fcl-monad)

